
FUNCTION Get_Total_Consumption (
   project_id_ IN VARCHAR2 ) RETURN VARCHAR
IS
   consumed_               NUMBER;
   planned_pur_req_        NUMBER;
   released_pur_req_       NUMBER;
   planned_pur_ord_        NUMBER;
   committed_              NUMBER;
   used_                   NUMBER;
   company_                VARCHAR2(20);
   consumption_point_      VARCHAR2(20);
   stmt_                   VARCHAR2(32000);
   multi_currency_enabled_ VARCHAR2(5);
   --(+)070712 MUNALK G1278069 - Subcontract Project Budget Check Modification (START)
   subcon_planned_comm_    NUMBER; 
   --(+)070712 MUNALK G1278069 - Subcontract Project Budget Check Modification (FINISH)
  

   CURSOR get_tot_consumed IS
      SELECT SUM(nvl(decode( planned_committed_status , 'PLANNED_PUR_ORD', DECODE(multi_currency_enabled_, 'TRUE', planned_committed_budget, planned_committed), 0),0)) planned_pur_ord,
             SUM(nvl(DECODE(multi_currency_enabled_, 'TRUE', committed_budget, committed),0)) committed,
             SUM(nvl(DECODE(multi_currency_enabled_, 'TRUE', used_budget, used),0)) used
       FROM project_connection_details_tab a, activity_tab b
      WHERE a.activity_seq = b.activity_seq
        AND b.project_id   = project_id_
        AND NOT (a.control_category = '4');

   $IF installed_component_sys.purch $THEN        
      CURSOR get_consumed IS
         SELECT SUM(nvl(decode( planned_committed_status , 'PLANNED_PUR_REQ', DECODE(multi_currency_enabled_, 'TRUE', planned_committed_budget, planned_committed), 0),0)) planned_pur_req,
                SUM(nvl(decode( planned_committed_status , 'RELEASED_PUR_REQ', DECODE(multi_currency_enabled_, 'TRUE', planned_committed_budget, planned_committed), 0),0)) released_pur_req
           FROM project_connection_details_tab a, activity_tab b
          WHERE a.activity_seq = b.activity_seq
            AND b.project_id   = project_id_
            AND NOT (a.control_category = '4')
            AND NOT EXISTS (SELECT *
                              FROM purchase_order_line_tab pol
                             WHERE pol.requisition_no  = a.keyref1
                               AND pol.req_line        = a.keyref2
                               AND pol.req_release     = a.keyref3
                               AND pol.activity_seq    = a.activity_seq
                               AND a.proj_lu_name IN ('PREQLINENP', 'PREQLINEP'));
   $END

   --(+)070712 MUNALK G1278069 - Subcontract Project Budget Check Modification (START)
    $IF installed_component_sys.subcon $THEN
      CURSOR get_consumed1 IS
         SELECT SUM(nvl(decode( a.proj_lu_name, 'SUBCONPROJ', DECODE(multi_currency_enabled_, 'TRUE', planned_committed_budget, planned_committed), 0),0)) subcon_planned_comm
           FROM project_connection_details_tab a, activity_tab b
          WHERE a.activity_seq = b.activity_seq
            AND b.project_id   = project_id_
            AND NOT (a.control_category = '4')
            AND NOT EXISTS (SELECT *
                              FROM purchase_order_line_tab pol
                             WHERE pol.requisition_no  = a.keyref1
                               AND pol.req_line        = a.keyref2
                               AND pol.req_release     = a.keyref3
                               AND pol.activity_seq    = a.activity_seq
                               AND a.proj_lu_name IN ('PREQLINENP', 'PREQLINEP'));
   $END
  --(+)070712 MUNALK G1278069 - Subcontract Project Budget Check Modification (FINISH)
BEGIN
   General_SYS.Init_Method(lu_name_, 'PROJ_BUDGET_CONTROL_RULES_API', 'Get_Total_Consumption');

   company_                := Project_API.Get_Company(project_id_);
   consumption_point_      := Proj_Budget_Control_Point_API.Encode(Proj_Comp_Bud_Con_API.Get_Consumption_Point(company_));
   multi_currency_enabled_ := NVL(Project_API.Get_Multi_Currency_Budgeting(project_id_),'FALSE');

   OPEN get_tot_consumed;
   FETCH get_tot_consumed INTO planned_pur_ord_, committed_, used_;
   CLOSE get_tot_consumed;

   $IF installed_component_sys.purch $THEN
      IF (consumption_point_ IN ('CREATE_PR_LINE', 'RELEASE_PR_LINE'))   THEN
         OPEN get_consumed;
         FETCH get_consumed INTO planned_pur_req_, released_pur_req_;
         CLOSE get_consumed;
      END IF;
   $END

  --(+)070712 MUNALK G1278069 - Subcontract Project Budget Check Modification (START)
    $IF  installed_component_sys.subcon $THEN
        OPEN get_consumed1;
         FETCH get_consumed1 INTO  subcon_planned_comm_ ;
         CLOSE get_consumed1;
   $END
  --(+)070712 MUNALK G1278069 - Subcontract Project Budget Check Modification (FINISH)

   CASE
      WHEN consumption_point_ = 'CREATE_PR_LINE' THEN
         consumed_ := nvl(planned_pur_req_,0) +
                      nvl(released_pur_req_,0) +
                      nvl(planned_pur_ord_,0) +
                      nvl(committed_,0) +
                      nvl(used_,0);
      WHEN consumption_point_ = 'RELEASE_PR_LINE' THEN
         consumed_ := nvl(released_pur_req_,0) +
                      nvl(planned_pur_ord_,0) +
                      nvl(committed_,0) +
                      nvl(used_,0);
      WHEN consumption_point_ = 'CREATE_PO_LINE' THEN
         consumed_ := nvl(planned_pur_ord_,0) +
                      nvl(committed_,0) +
                      nvl(used_,0);
      WHEN consumption_point_ = 'RELEASE_PO_LINE' THEN
         consumed_ := nvl(committed_,0) +
                      nvl(used_,0);
      ELSE
         NULL;
   END CASE;
   
   --(+)070712 MUNALK G1278069 - Subcontract Project Budget Check Modification (START)
   IF(subcon_planned_comm_ IS NOT NULL) THEN
      consumed_ := consumed_ + subcon_planned_comm_;
   END IF;
   --(+)070712 MUNALK G1278069 - Subcontract Project Budget Check Modification (FINISH)
   RETURN nvl(consumed_,0);

END Get_Total_Consumption;
